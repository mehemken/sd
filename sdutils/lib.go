package sdutils

import (
	"errors"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"text/tabwriter"

	k8sioerrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////

type TargetSecret struct {
	Namespace string
	Secret    string
}

////////////////////////////////////////////////////////////
// Public Funcs
////////////////////////////////////////////////////////////

// Pretty_tabs: Takes the downloaded secret and prints it in tabular form.
func Pretty_tabs(secretData map[string][]byte, target TargetSecret) {

	// Header
	header := fmt.Sprintf("\nNamespace: %s -- Secret: %s", target.Namespace, target.Secret)
	fmt.Println(header)
	fmt.Println(strings.Repeat("-", len(header)))

	// Init new tabwriter
	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 0, 8, 0, '\t', 0)

	// Loop through data
	for k, v := range secretData {
		line := fmt.Sprintf("%s: \t%s", k, v)
		fmt.Fprintln(w, line)
	}
	w.Flush()
	fmt.Println("")
}

// get_secret: Calls the k8s API to get and decode and print the target secret.
func Get_secret(namespace string, secretName string) (map[string][]byte, error) {

	// Get the user's kubeconfig
	kubeconfig, err := get_kubeconfig()
	if err != nil {
		return make(map[string][]byte), err
	}

	// Use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		return make(map[string][]byte), err
	}

	// Create the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return make(map[string][]byte), err
	}

	// Get the secret
	result, err := clientset.CoreV1().Secrets(namespace).Get(secretName, metav1.GetOptions{})
	if k8sioerrors.IsNotFound(err) {
		msg := fmt.Sprintf("Secret %s in namespace %s not found\n", secretName, namespace)
		return make(map[string][]byte), errors.New(msg)
	} else if statusError, isStatus := err.(*k8sioerrors.StatusError); isStatus {
		msg := fmt.Sprintf("Error getting secret %s in namespace %s: %v\n", secretName, namespace, statusError.ErrStatus.Message)
		return make(map[string][]byte), errors.New(msg)
	} else if err != nil {
		return make(map[string][]byte), err
	}

	return result.Data, nil
}

// Parse_flags: parses flags and returns a struct
func Parse_flags() (TargetSecret, error) {

	// Flags
	target := TargetSecret{}
	flag.Parse()
	args := flag.Args()

	// Only two args allowed
	if len(args) == 2 {
		target.Namespace = args[0]
		target.Secret = args[1]
	} else {
		return target, errors.New("Needs two arguents: [namespace] [secretName]\nTry -h")
	}
	return target, nil
}

////////////////////////////////////////////////////////////
// Private Funcs
////////////////////////////////////////////////////////////

// home_dir: Gets the $HOME diretory of the user.
func home_dir() (string, error) {
	home := os.Getenv("HOME")
	if home == "" {
		return home, errors.New("HOME directory not found. Are you on Windows? Try Linux or Mac.")
	}
	return home, nil
}

// get_kubeconfig: Returns a *string containing the location of the kubeconfig file.
func get_kubeconfig() (*string, error) {
	var kubeconfig string
	home, err := home_dir()
	if err != nil {
		return &kubeconfig, err
	}
	kubeconfig = filepath.Join(home, ".kube", "config")
	if _, err := os.Stat(kubeconfig); err != nil {
		msg := fmt.Sprintf("Your kubeconfig is not at %s. Only this location is currently supported.", kubeconfig)
		return &kubeconfig, errors.New(msg)
	}
	return &kubeconfig, nil
}
