// sd: secret decoder decodes kubernetes secrets and prints them to the screen
package main

import (
	"log"
	"sd/sdutils"
)

////////////////////////////////////////////////////////////
// Main
////////////////////////////////////////////////////////////

func main() {

	// Flags
	target, err := sdutils.Parse_flags()
	if err != nil {
		log.Fatal(err)
	}

	// Decode secrets
	result, err := sdutils.Get_secret(target.Namespace, target.Secret)
	if err != nil {
		log.Fatal(err)
	}

	// Outputs
	sdutils.Pretty_tabs(result, target)

}
