# Secret Decoder

Inspired by the [secret decoder](https://github.com/RaymondArias/SecretDecoder) written by Ray Arias.

Usage:

    $ sd [namespace] [secretName]

And if
  - Your ~/.kube/config is in the right place
  - You have appropriate permissions

you will see the secrets you've requested.
